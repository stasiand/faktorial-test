package cz.cvut.fel.ts1;

public class stasiand {
   public long factorial(int n)
   {
       long result = 1;
       for (int i = 1; i <= n; i++)
       {
           result *= i;
       }
       return result;
   }
    public long factorialRecursive(int n)
    {
        if (n == 0) {
            return 1;
        } else {
            return n * factorialRecursive(n - 1);
        }
    }
}


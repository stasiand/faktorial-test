package cz.cvut.fel.ts1;
import org.junit.jupiter.api.Test;
import static
        org.junit.jupiter.api.Assertions.*;

public class stasiandTest {
    @Test
    public void factorialTest(){
        stasiand stasiand = new stasiand();
        assertEquals(120, stasiand.factorial(5));
        assertEquals(1, stasiand.factorialRecursive(0));
    }
}
